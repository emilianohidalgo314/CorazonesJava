import java.util.Scanner;
import java.util.ArrayList;
import java. util.Iterator;


public class Jugador {
    int num;    //Número de jugador
    int puntos;
    boolean humano;
    ArrayList<Carta> cartas = new ArrayList<Carta>();

    public Jugador(int num){
        this.num = num;
        puntos = 0;
    }

    public boolean tieneCarta(String s){ 
        for (Carta c : cartas) {
            if(c.nombre.equals(s))
                return true;
        }
        return false;
    }

    public void generarPuntos(Carta[] monto) {
        for (Carta carta : monto) {
            if(carta.palo == 'C')
                puntos += 1;
            if (carta.palo == 'P' && carta.valor == 12){
                puntos += 13;
            }
        }
    }
    public void agregarCarta(Carta c) { cartas.add(c);}

    public boolean esHumano() { return humano;}

    public int getNum() { return num;}

    public Carta jugada(Scanner sc, Carta c) {    //Humano
        return null;
    }

    public Carta jugada(Carta c) {  //IA 
        return null;
    }

    public Carta primerJugada(Scanner sc) {    //Humano
        return null;
    }

    public Carta primerJugada() {  //IA 
        return null;
    }

    public void jugarCarta(Carta c) {
        for (Iterator<Carta> iter = cartas.listIterator(); iter.hasNext(); ) {
            Carta temp = iter.next();
            if (temp == c) 
                iter.remove();
        }
    }
}
