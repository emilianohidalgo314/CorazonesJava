import java.util.Scanner;

public class Corazones{
    public static void main(String[] args) {
        Croupier croupier = new Croupier();
        Carta[] baraja = new Carta[52];    
        java.util.Scanner sc = new Scanner(System.in);
        
        croupier.inicializarCartas(baraja);
        croupier.shuffle(baraja);    //Barajeo de cartas
        
        System.out.println("==========Corazones==========\nIngresar número de jugadores");
        Jugador[] jugadores = new Jugador[4];
        
        asignarJugadores(sc.nextInt(),jugadores);
        sc.nextLine();  //Se come línea vacía
        
        croupier.repartir(jugadores, baraja);
        int numJug = ordenarJugadores(jugadores) - 1;   //Número del primer jugador en el arreglo
        for(int i = 0; i < 13; i++){    //Loop de partida
            Carta[] monto = new Carta[4];
            int max = 0;    //Puntaje más alto de cada ronda
            Jugador ganador = jugadores[0]; //Inicialización de la variable
            String cartasJugadas = "";
            Carta cartaEnJuego = new Carta("2T",0x1F0D2); //Inicialización de variable

            for (int j = 0; j < 4; j++){    //Loop de turnos

                int indexJugador = numJug + j - (numJug + j >= 4 ? 4: 0 );
                Carta cJugada;
                if(j == 0){  //Primer turno
                    if(jugadores[indexJugador].esHumano())
                        cJugada = jugadores[indexJugador].primerJugada(sc); 
                    else 
                        cJugada = jugadores[indexJugador].primerJugada(); 
                    cartaEnJuego = cJugada;
                }
                else{
                    if(jugadores[indexJugador].esHumano())
                        cJugada = jugadores[indexJugador].jugada(sc, cartaEnJuego); 

                    else 
                        cJugada = jugadores[indexJugador].jugada(cartaEnJuego);
                }
                cartasJugadas += cJugada + " ";
                System.out.println("Cartas jugadas: " + cartasJugadas +"\n");
                if(cJugada.valor > max){
                    max = cJugada.valor;
                    ganador = jugadores[indexJugador];
                    }
                monto[j] = cJugada; 
                jugadores[indexJugador].jugarCarta(cJugada);
                }
            System.out.println("El jugador " + ganador.num + " se lleva el monto!\n" ); 
            System.out.println(i != 12 ? "El jugador " + ganador.num + " empieza\n": "Partida Terminda!" );   
            ganador.generarPuntos(monto);
            numJug = ganador.num - 1;
            }
        System.out.println("El jugador " + encontrarGanador(jugadores).num + " ha ganado!");
    }

    public static Jugador encontrarGanador(Jugador[] jugadores) {
        int min = 100;
        Jugador ganador = jugadores[0]; //Inicialización de la variable
        for (Jugador j : jugadores) {
            if (j.puntos < min){
                min = j.puntos;
                ganador = j;
            }
        }
        return ganador;
    }

    public static int ordenarJugadores(Jugador[] jugadores) {
        for (Jugador j : jugadores) {
            if (j.tieneCarta("2T")){
                System.out.println("El jugador " + j.getNum() + " empieza\n");
                return j.getNum();
            }
        }
        return 5; //Error
    }
    public static void asignarJugadores(int numJug, Jugador[] jugadores) {
        //Asignación humanos
        for (int i = 0; i < numJug; i++){
            jugadores[i] = new JugadorH(i+1);
        }
        //Asignación de IA
        for (int j = 0; j < 4 - numJug; j++){
            jugadores[j + numJug] = new IA(j+1);
        }
    }
   
}

