import java.util.Scanner;
public class JugadorH extends Jugador {

    public JugadorH(int num){
        super(num);
        humano = true;
    }
    @Override
    public Carta jugada(Scanner sc,Carta enJuego) {
        System.out.println("Turno del jugador " + this.num + "\n");
        System.out.println(mostrarCartas());
        for(;;){
            String cartaAJugar = sc.nextLine();
            if(validarCarta(cartaAJugar,enJuego)){
                return encontrarCarta(cartaAJugar);
            }
        }
    }

    @Override
    public Carta primerJugada(Scanner sc) {
        System.out.println("Turno del jugador " + this.num + "\n");
        System.out.println(mostrarCartas());
        for(;;){
            String cartaAJugar = sc.nextLine();
            if(tieneCarta(cartaAJugar)){
                return encontrarCarta(cartaAJugar);
            }
            System.out.println("No tienes esa carta!");
        }
    }

    public String mostrarCartas() {
        String mano = "";
        for (Carta c : cartas) {
           mano += c + " ";
        }
        return mano;
    }

    public Carta encontrarCarta(String nomC) {
        for (Carta carta : cartas) {
            if(carta.nombre.equals(nomC))
                return carta;
        }
        return null;
    }

    public boolean validarCarta(String cartaAJugar, Carta enJuego){
        if(!(tieneCarta(cartaAJugar))){
            System.out.println("No tienes esa carta!");
            return false;
        }
        Carta cAJugar = encontrarCarta(cartaAJugar);
        if (cAJugar.palo == enJuego.palo || cAJugar.palo == 'C')
            return true;
        System.out.println("Palo equivocado!");
        return false;
    }
}