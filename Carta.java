public class Carta{
    String nombre;
    String unicode;
    int valor;
    char palo;

    public Carta(String nombre, int uni){
        this.nombre = nombre;
        unicode = new StringBuilder().appendCodePoint(uni).toString();
        palo = nombre.charAt(nombre.length() - 1);
        valor = generarValor(nombre.substring(0, nombre.length() - 1));
        }
    

    public int generarValor(String s){
        switch(s){
            case "J": return 11;
            case "Q": return 12;
            case "K": return 13;
            case "A": return 14;
            default: return Integer.parseInt(s);
        }
    }


    public String getNombre() { return nombre; }

    @Override
    public String toString() {  return unicode; }

    
}