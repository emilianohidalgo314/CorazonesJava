import java.util.Random;


public class Croupier{
    public void shuffle(Carta[] baraja) {
        Random random = new Random();
        for (int i = 52; i > 1; i--) {
            swap(baraja, i - 1, random.nextInt(i));
        }
    }

    private void swap(Carta[] baraja, int i, int j) {
        Carta temp = baraja[i];
        baraja[i] = baraja[j];
        baraja[j] = temp;   
    }

    public void repartir(Jugador[] jugadores, Carta[] baraja) {
        for (int i = 0; i < 13; i++){
            jugadores[0].agregarCarta(baraja[i*4]);
            jugadores[1].agregarCarta(baraja[i*4+1]);
            jugadores[2].agregarCarta(baraja[i*4+2]);
            jugadores[3].agregarCarta(baraja[i*4+3]);
        }
    }

    public void inicializarCartas(Carta[] baraja) {
        //Picas
        baraja[0] = new Carta("AP",0x1F0A1);
        baraja[1] = new Carta("2P",0x1F0A2);
        baraja[2] = new Carta("3P",0x1F0A3);
        baraja[3] = new Carta("4P",0x1F0A4);
        baraja[4] = new Carta("5P",0x1F0A5);
        baraja[5] = new Carta("6P",0x1F0A6);
        baraja[6] = new Carta("7P",0x1F0A7);
        baraja[7] = new Carta("8P",0x1F0A8);
        baraja[8] = new Carta("9P",0x1F0A9);
        baraja[9] = new Carta("10P",0x1F0AA);
        baraja[10] = new Carta("JP",0x1F0AB);
        baraja[11] = new Carta("QP",0x1F0AD);
        baraja[13] = new Carta("KP",0x1F0AE);

        //Trebol
        baraja[14] = new Carta("AT",0x1F0D1);
        baraja[15] = new Carta("2T",0x1F0D2);
        baraja[16] = new Carta("3T",0x1F0D3);
        baraja[17] = new Carta("4T",0x1F0D4);
        baraja[18] = new Carta("5T",0x1F0D5);
        baraja[19] = new Carta("6T",0x1F0D6);
        baraja[20] = new Carta("7T",0x1F0D7);
        baraja[21] = new Carta("8T",0x1F0D8);
        baraja[22] = new Carta("9T",0x1F0D9);
        baraja[23] = new Carta("10T",0x1F0DA);
        baraja[24] = new Carta("JT",0x1F0DB);
        baraja[25] = new Carta("QT",0x1F0DD);
        baraja[27] = new Carta("KT",0x1F0DE);

        //Corazones
        baraja[28] = new Carta("AC",0x1F0B1);
        baraja[29] = new Carta("2C",0x1F0B2);
        baraja[30] = new Carta("3C",0x1F0B3);
        baraja[31] = new Carta("4C",0x1F0B4);
        baraja[32] = new Carta("5C",0x1F0B5);
        baraja[33] = new Carta("6C",0x1F0B6);
        baraja[34] = new Carta("7C",0x1F0B7);
        baraja[35] = new Carta("8C",0x1F0B8);
        baraja[36] = new Carta("9C",0x1F0B9);
        baraja[37] = new Carta("10C",0x1F0BA);
        baraja[38] = new Carta("JC",0x1F0BB);
        baraja[39] = new Carta("QC",0x1F0BD);
        baraja[41] = new Carta("KC",0x1F0BE);

        //Diamantes
        baraja[42] = new Carta("AD",0x1F0B1);
        baraja[43] = new Carta("2D",0x1F0B2);
        baraja[44] = new Carta("3D",0x1F0B3);
        baraja[45] = new Carta("4D",0x1F0B4);
        baraja[46] = new Carta("5D",0x1F0B5);
        baraja[47] = new Carta("6D",0x1F0B6);
        baraja[48] = new Carta("7D",0x1F0B7);
        baraja[49] = new Carta("8D",0x1F0B8);
        baraja[50] = new Carta("9D",0x1F0B9);
        baraja[51] = new Carta("10D",0x1F0BA);
        baraja[40] = new Carta("JD",0x1F0BB);
        baraja[12] = new Carta("QD",0x1F0BD);
        baraja[26] = new Carta("KD",0x1F0BE);  
    }
}