public class IA extends Jugador {

    public IA(int num){
        super(num);
        humano = false;
    }
    
    @Override
    public Carta jugada(Carta c) {
        for (Carta carta : cartas) {
            if (carta.palo == c.palo)  return carta;
        }

        for (Carta cartaC : cartas) {
            if (cartaC.palo == 'C') return cartaC;   
        }
        return cartas.get(0);
    }

    @Override
    public Carta primerJugada(){
        return cartas.get(0);
    }
}